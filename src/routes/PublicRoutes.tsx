import {FC} from 'react';
import {Navigate} from 'react-router-dom';
import {IRoutesProps} from 'utils/interfaces';

export const PublicRoutes: FC<IRoutesProps> = ({
    children,isAuthenticated
}) => <>{isAuthenticated ? <Navigate to='/'/> : children}</>;