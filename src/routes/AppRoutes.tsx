import {Suspense, useEffect, useState} from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import {PublicRoutes} from './PublicRoutes';
import {PrivateRoutes} from './PrivateRoutes';
import {DashboardRouters} from './DashboardRouters';

import {useAppSelector} from 'store/hooks'
import * as authSlice from 'store/slices/Auth/auth.slice'
import { routerPublic } from './routers';

export  const AppRoutes = () => {
   
    const [isLoading, setIsLoading] = useState(true);
    const {auth:{isAuthenticate}} = useAppSelector(authSlice.selectAuth)
   
    useEffect(() => {
        setTimeout(() => {
            setIsLoading (false)
        }, 3000)
    }, []);

    if(isLoading) return null;
    
    return (
        <BrowserRouter> 
            <Routes>
                {
                    routerPublic.map(({to,Component}, index) => (
                        <Route
                           key = {index}
                           path = {to}
                           element = {
                               <PublicRoutes isAuthenticated={isAuthenticate} >
                                    <Suspense fallback = 'Cargando'>
                                        <Component/>
                                    </Suspense>
                               </PublicRoutes>
                           } 
                        />
                    ))
                }
                <Route
                    path='/*'
                    element={
                        <PrivateRoutes isAuthenticated={isAuthenticate}>
                            <Suspense fallback='Cargando'>
                                <DashboardRouters />
                            </Suspense>
                        </PrivateRoutes>
                    }
                />
            </Routes>
        </BrowserRouter>
    )
}