import {Suspense} from 'react';
import {Routes, Route, Navigate} from 'react-router-dom'
import {routerPrivate} from './routers';


export const DashboardRouters = () => {
    return (
        <Routes>
            {
                routerPrivate.map(({to, Component})=> (
                    <Route
                        key={to}
                        path={to}
                        element= {
                            <Suspense fallback='cargando'>
                                <Component/>
                            </Suspense>
                        }
                    />
                ))
            }
            <Route path='*' element={<Navigate to={routerPrivate[0].to} replace/>}/>

        </Routes>

    )
}

export default DashboardRouters
