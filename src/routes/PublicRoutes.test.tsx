import { mount, shallow } from 'enzyme';
import {PublicRoutes} from './PublicRoutes'
import toJson from 'enzyme-to-json';


describe('Test PrivateRoutes', () => {

    test ('Debe mostrarse correctamente' , () => {
        const children = <p>Page de crear partida</p>

        const wrapperPublic = shallow (
            <PublicRoutes isAuthenticated={false}>
            {children}
            </PublicRoutes>
        );
        expect(toJson(wrapperPublic)).toMatchSnapshot
    });

    test('Debe redirigir si no esta autenticado', () => {
        const children = <p>Page de crear partida</p>

        const wrapperPublic = shallow (
            <PublicRoutes isAuthenticated={true}>
            {children}
            </PublicRoutes>
        );
        expect(toJson(wrapperPublic)).toMatchSnapshot();
        expect(null).toBe(toJson(wrapperPublic).children[0].children)

    })
})
