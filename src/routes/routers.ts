import {lazy} from 'react';
import {IRouter} from 'utils/interfaces'

    const LazyNewGame = lazy(() => import('pages/public/NewGamePage/NewGamePage.components'))
    const LazyStartPlayer = lazy(() => import('pages/public/StartPlayerPage/StartPlayerPage.components'))

    const LazyTablePoker = lazy(() => import('pages/private/TablePokerPage/TablePokerPage.components'));

    export const routerPublic: IRouter [] = [
        {
            path: 'newGame',
            to: '/new-game',
            name: 'NewGame',
            Component:LazyNewGame
        },
        {
            path: 'startPlayer',
            to: '/start-Player',
            name: 'StartPlayer',
            Component:LazyStartPlayer
        },
    ]    
    
    export const routerPrivate: IRouter [] = [
        
        {
            path: '',
            to: '/',
            name: '',
            Component:LazyTablePoker
        },
        {
            path: 'tablePoker',
            to: '/table-poker',
            name: 'tablePoker',
            Component:LazyTablePoker
        },
    ]