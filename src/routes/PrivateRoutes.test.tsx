import { mount, shallow } from 'enzyme';
import {PrivateRoutes} from './PrivateRoutes'
import toJson from 'enzyme-to-json';


describe('Test PublicRoutes', () => {

    test ('Debe mostrarse correctamente' , () => {
        const children = <p>Table Poker</p>

        const wrapperPublic = shallow (
            <PrivateRoutes isAuthenticated={true}>
            {children}
            </PrivateRoutes>
        );
        expect(toJson(wrapperPublic)).toMatchSnapshot
    });

    test('No debería acceder a las rutas privadas porque no esta autenticado', () => {
        const children = <p>Table Poker</p>

        const wrapperPublic = shallow (
            <PrivateRoutes isAuthenticated={false}>
            {children}
            </PrivateRoutes>
        );
        // expect(toJson(wrapperPublic)).toMatchSnapshot();
        // expect(null).toBe(toJson(wrapperPublic).children[0].children)
        expect('<Navigate />').toBe(wrapperPublic.text())

    })
})