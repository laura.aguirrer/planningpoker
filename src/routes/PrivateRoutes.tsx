import {FC} from 'react';
import {Navigate} from 'react-router-dom';
import {IRoutesProps} from 'utils/interfaces';

export const PrivateRoutes: FC<IRoutesProps> = ({
    children,isAuthenticated
}) => <>{isAuthenticated ? children : <Navigate to='/new-game' /> }</>;