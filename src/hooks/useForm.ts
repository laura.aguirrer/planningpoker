import {FormEvent, useState} from 'react'

interface IuseForm <I> {
    defaultValues: I;
    resolve?:any;
}

export function useForm<T extends object>({defaultValues, resolve}: IuseForm<T>)  {
    const [formValue, setFormValue] = useState<T>(defaultValues);

    const handleInputChange = ({currentTarget} : FormEvent<HTMLInputElement>)=> {
        if (currentTarget.type == 'file') {
            return setFormValue (
                {
                    ...formValue,
                    [currentTarget.name]: currentTarget.files && currentTarget.files[0]
                }
            )
        } else if(currentTarget.type == 'checkbox') {
            return setFormValue (
                {
                    ...formValue,
                    [currentTarget.name]: currentTarget.checked
                }
              )  
        } else {
           return  setFormValue ( {
                    ...formValue,
                    [currentTarget.name]: currentTarget.value
                }
            )
        }

    }

    const clearFormValues = () => setFormValue(defaultValues);

    return {
        formValue,
        handleInputChange,
        setFormValue,
        clearFormValues 
    };
}





//     const INPUT_TYPES: any = {
//         'file': ({currentTarget} : FormEvent<HTMLInputElement>) => {
            
//         },
//         'checkbox': ({currentTarget} : FormEvent<HTMLInputElement>) => {
            
//         },
        
//     }
//     const INPUT_DEFAULT:any = {
//         'textbox': ({currentTarget} : FormEvent<HTMLInputElement>) => {
               
//         }
//     }

//    
// }
