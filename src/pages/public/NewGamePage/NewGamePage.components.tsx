import {FormNewGame} from 'components/organism'
import{Layout} from 'components/templates'


export const NewGamePage = () => {
   
   return(
       <Layout
       isFigCaption
       titleFigCaption = 'Crear Partida'
       classHeader='layout__header_figure--new_game'
       >  
        <FormNewGame/>
           
       </Layout>
   )
}

export default NewGamePage