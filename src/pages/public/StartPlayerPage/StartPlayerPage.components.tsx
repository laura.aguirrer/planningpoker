import { useEffect } from 'react'
import {TablePokerCardsValues} from 'components/organism'
import{Layout} from 'components/templates'
import {useAppDispatch, useAppSelector} from 'store/hooks'
import {setModal , selectGame} from 'store/slices/Game'

import './StartPlayerPage.components.scss'


export const StartPlayerPage = () =>  {
    const distpach = useAppDispatch();
    const {game: {isModal, typeModal}} = useAppSelector(selectGame)
    // const nameGame = localStorage.getItem('namePartida') || ''; //temporal
    useEffect(() => {
     distpach(setModal({isModal: true, typeModal: 'start-player'}));
     return () => {
        distpach(setModal({isModal: false, typeModal: ''}));  
     }
    }, [])

    return(
        <Layout isModal={isModal} typeModal={typeModal} >   
            <TablePokerCardsValues/>       
        </Layout>
    )
}


export default StartPlayerPage