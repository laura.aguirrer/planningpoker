import { TablePokerCardsValues } from 'components/organism'
import { Layout } from 'components/templates'
import {useAppSelector} from 'store/hooks'
import * as authSlice from 'store/slices/Auth/auth.slice'
import * as gameSlice from 'store/slices/Game/game.slice'
import {createNameAvatarCharacters} from 'utils/helpers'
import './TablePokerPage.components.scss'


export const TablePokerPage = () =>  {
  const{auth: {namePlayer}} = useAppSelector(authSlice.selectAuth) 
  const{game: {nameRoom, isModal,typeModal}} = useAppSelector(gameSlice.selectGame) 
    return (
        <Layout isFigCaption isModal={isModal} typeModal={typeModal}
        isSubHeader nameAvatar={createNameAvatarCharacters(namePlayer)} 
        nameGame={nameRoom
        }>
            <TablePokerCardsValues isShowOptionsTable/>
        </Layout>
    )
}


export default TablePokerPage
