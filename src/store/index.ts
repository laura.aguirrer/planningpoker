import { configureStore } from '@reduxjs/toolkit';
import auth from 'store/slices/Auth/auth.slice'
import game from 'store/slices/Game/game.slice'

const store = configureStore({
    reducer : {
        auth,
        game
    }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export default store
