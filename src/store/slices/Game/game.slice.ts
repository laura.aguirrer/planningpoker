import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';
import { IRoom, IModal, IPlayer, IOperationResult } from 'utils/interfaces';


const initialStateGame: IRoom = {
    id: '',
    nameRoom: '',
    isVotation: false,
    isRevelationCards: false,
    isModal: false,
    typeModal: '',
    players: [
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-addgrsrss',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdyufys',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdfytufyu',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'dvtydtbyus',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'nyufnynyuun',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'myiuologt',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },
        {
            id: 'asdfdf-adfasd-dsaaas',
            namePlayer: 'AAAA',
            rolPlayer: 'player',
            valuePlayer: 2
        },{
            id: 'asdfrht',
            namePlayer: 'BBBB',
            rolPlayer: 'player',
            valuePlayer: 3
        },{
            id: 'abkmytbmytb',
            namePlayer: 'CCCC',
            rolPlayer: 'viewer',
            valuePlayer: null
        }
    ],
    operations: {
        isResult: false,
        valuesCount: [],
        average: 0
    }
}

const gameSlice = createSlice({
    name: 'Game',
    initialState: initialStateGame,
    reducers: {
        createRoom(state, action: PayloadAction<IRoom>) {
            state.id = action.payload.id;
            state.nameRoom = action.payload.nameRoom;
        },
        addPlayers(state, action: PayloadAction<IPlayer>) {
            if (state.players.length + 1 > 20) return;
            state.players?.push({ ...action.payload })

        },
        setModal(state, action: PayloadAction<IModal>) {
            state.isModal = action.payload.isModal;
            state.typeModal = action.payload.typeModal;

            //state = {...state, ...action.payload} ---> corto
        },
        setStateVoted(state, action: PayloadAction<{ isVotation: boolean }>) {
            state.isVotation = action.payload.isVotation;
        },
        setStateRevelateCards(state, action: PayloadAction<{ isRevelationCards: boolean }>) {
            state.isRevelationCards = action.payload.isRevelationCards;
        },
        setResetVotedPlayers(state, action: PayloadAction<{ valuePlayer: number | string | null }>) {
            state.players.map(player => player.valuePlayer = action.payload.valuePlayer)
        },
        setStateVotedByIdPlayer(state, action: PayloadAction<{ id: string, valuePlayer: number | string | null }>) {
            state.players.map(
                player => player.id === action.payload.id ? player.valuePlayer = action.payload.valuePlayer : player)
        },
        setOperations(state, action: PayloadAction<IOperationResult >){
            state.operations = action.payload.operations
        },
        setAvarege(state, action: PayloadAction<{avarage:number}>){
            state.operations = {
                ...state.operations,
                average: action.payload.avarage
            }
        }
    }
    
});

export const {
    createRoom,
    addPlayers,
    setModal,
    setStateVoted,
    setStateRevelateCards,
    setResetVotedPlayers,
    setStateVotedByIdPlayer,
    setOperations,
    setAvarege
} = gameSlice.actions;

export const selectGame = (state: RootState) => state;
export default gameSlice.reducer