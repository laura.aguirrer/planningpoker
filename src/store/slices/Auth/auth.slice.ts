import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from 'store'

interface ICardsValues {
    value: number | string;
    id: number | string;
    isCheked:boolean;
}
interface ICards {
    cardsValues:ICardsValues[]
}

interface IAuth  extends ICards {
    id: string,
    isAuthenticate: boolean,
    namePlayer: string,
    rolPlayer: string,
    
}

const initialStateAuth: IAuth = {
    id: '',
    isAuthenticate: false,
    namePlayer: '',
    rolPlayer:'',
    cardsValues: []
}

const authSlice = createSlice({
    name: 'Auth',
    initialState: initialStateAuth,
    reducers: {
        setAuthenticate(state, action: PayloadAction<IAuth>) {
            state.isAuthenticate = action.payload.isAuthenticate;
            state.namePlayer = action.payload.namePlayer;
            state.rolPlayer = action.payload.rolPlayer;
            state.id = action.payload.id;
            state.cardsValues = action.payload.cardsValues;

        },
        setChekedCards(state, action:PayloadAction<ICardsValues>) {
            state.cardsValues= state.cardsValues?.map(
                card => card.id === action.payload.id ? {...action.payload} : {...card, isCheked:false}
            )
        },
        setResetCheckedCards(state) {
            state.cardsValues?.map(card => card.isCheked = false)
        }, 
        
    }
});

export const {
    setAuthenticate,
    setChekedCards,
    setResetCheckedCards
} = authSlice.actions;

export const selectAuth = (state: RootState) => state;
export default authSlice.reducer;
