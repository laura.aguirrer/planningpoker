import { IPlayer, IValuesCount, IClearValue } from 'utils/interfaces';


export const createNameAvatarCharacters = (texto: string): string => {
    let character: string;
    if (texto.includes(' ')) {
        let textTemp = texto.split(' ');
        if (textTemp[1].toString().trim().length === 0) {
            character = texto.charAt(0).toUpperCase();
            character += texto.charAt(1).toUpperCase();
        } else {
            character = textTemp[0].charAt(0).toUpperCase();
            character += textTemp[1].charAt(0).toUpperCase();
        }
    } else {
        character = texto.charAt(0).toUpperCase();
        character += texto.charAt(1).toUpperCase();
    }


    return character;

}

export const isVoted = (players: IPlayer[]): boolean => {

    if (players.some(player => player.valuePlayer !== null)) return true;

    return false;
}

export const countValues = (players: IPlayer[]):IValuesCount[] => {
    let valuesArray: string[] | number[] | any[] = [];
    let valuesMap: any = {};
    const valuesCount: IValuesCount[] = [];
    players.map((player) => player['valuePlayer'] !== null && valuesArray.push(player['valuePlayer'])); // convierte de arrayObjects a array
    valuesArray.forEach(value => valuesMap[value] = valuesMap[value] + 1 || 1); // map {} valor x tiene y votos

    for (const [key, value] of Object.entries(valuesMap)) {
        valuesCount.push({ value: key, count: value })
    }

    return valuesCount
}
export const pluralOrSingular = (value: number | unknown): string => {
   return value! > 1 ? `${value} Votos` : `${value} Voto`
}

export const getAverage = (valuesCount:  IValuesCount[]): number => {
    const clearValuesCount: IClearValue[] = []; 
    let countValues = 0;
    let countVotes = 0;
    valuesCount.filter((valueCount) => valueCount.value !== '☕️' && valueCount.value !== '?' ? clearValuesCount.push(valueCount) : null);
    clearValuesCount.forEach(({value,count}) => { countValues += Number(value)*Number(count); countVotes += Number(count); } );
    return Number((countValues / countVotes).toFixed(2));
}
  
