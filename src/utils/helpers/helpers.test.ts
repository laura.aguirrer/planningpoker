import { createNameAvatarCharacters, isVoted, countValues, pluralOrSingular, getAverage } from './'
import { IPlayer, IValuesCount, IClearValue } from 'utils/interfaces';


describe('pruebas en helpers', () => {
    const playerVotes: IPlayer[] = [
        {
            id: '1',
            namePlayer: 'AAA',
            rolPlayer: 'player',
            valuePlayer: 55
        },

        {
            id: '2',
            namePlayer: 'BBB',
            rolPlayer: 'viewer',
            valuePlayer: null
        },

        {
            id: '3',
            namePlayer: 'CCC',
            rolPlayer: 'player',
            valuePlayer: null
        },

        {
            id: '4',
            namePlayer: 'DDD',
            rolPlayer: 'player',
            valuePlayer: 13
        }
    ];

    const votesExpect: IValuesCount[] = [
        {
            value: '13',
            count: 1
        },
        {
            value: '55',
            count: 1,
        },
    ];

    test('test en creando un avatar con nombre compuesto', () => {
        const avatar = createNameAvatarCharacters('Valentina Aguirre');
        expect('VA').toBe(avatar)
    });

    test('test en creando un avatar con un solo nombre', () => {
        const avatar = createNameAvatarCharacters('Valentina');
        expect('VA').toBe(avatar)
    });


    test('test in isVoted < 1 player is true', () => {
        const voteNumber = isVoted(playerVotes);
        // console.log(voteNumber)
        expect(true).toBe(voteNumber);
    })

    test('test in countValues', () => {
        const countsVoted = countValues(playerVotes);
        expect(votesExpect).toEqual(countsVoted)

    })

    test('test in pluralOrSingular', () => {
        const value = pluralOrSingular(5);
        expect('5 Votos').toEqual(value)

    })

    test('test in pluralOrSingulart', () => {
        const votes = getAverage(votesExpect)
        expect(34).toEqual(votes)
    })
})