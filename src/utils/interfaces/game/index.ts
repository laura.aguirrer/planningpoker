//JUGADOR
export interface IPlayer {
    id: string
    namePlayer: string;
    rolPlayer: string;
    valuePlayer: number | string | null;
}
//INTERFACE MODAL
export interface IModal {
    isModal?: boolean;
    typeModal?: '' | 'invite' | 'start-player'
}
// INTERFACE VALUES
export interface IValuesCount {
    value: number | string,
    count: number | unknown
}

export interface IClearValue {
    value: number | string,
    count: number | unknown
}

//INTERFACE OPERACIONES
export interface IOperationResult {
    operations: {
        isResult: boolean,
        valuesCount: IValuesCount[],
        average: number
    }
}
//SALA DEL JUEGO    
export interface IRoom extends IModal, IOperationResult {
    id: string;
    nameRoom: string;
    isVotation?: boolean;
    isRevelationCards?: boolean;
    players: IPlayer[];
}
