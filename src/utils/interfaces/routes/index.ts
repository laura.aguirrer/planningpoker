import  {ReactNode, LazyExoticComponent } from 'react';
export type JSXComponent = () => JSX.Element;

export interface IRoutesProps {
    children: ReactNode | ReactNode[];
    isAuthenticated: boolean;
}

export interface IRouter {
    to: string;
    path: string;
    Component: LazyExoticComponent<JSXComponent> | JSXComponent;
    name: string;
}