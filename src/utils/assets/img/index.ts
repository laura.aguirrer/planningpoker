import fichaPokerImg from './ficha-de-poker.svg';
import logoImg from './Logo.svg';
import iconClose from './icon_close.svg'

export {
    fichaPokerImg,
    logoImg,
    iconClose
}