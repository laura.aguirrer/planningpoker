import { FC } from 'react';
import { AImage, AButton } from 'components/atoms'
import { fichaPokerImg } from 'utils/assets/img'
import {setModal} from 'store/slices/Game'
import {useAppDispatch} from 'store/hooks'
import './Header.components.scss'

interface IHeader {
    isFigCaption?: boolean;
    titleFigCaption?: string;
    isSubHeader?: boolean;
    nameGame?: string;
    nameAvatar?: string;
    classHeader?: string;
}

export const Header: FC<IHeader> = ({ isFigCaption, titleFigCaption, isSubHeader=false, nameGame, nameAvatar, classHeader }) => {
    const distpach = useAppDispatch();
    const onClickInvitate= () => {
       distpach(setModal({isModal:true, typeModal: 'invite' }));
    }
    return (
      <header className='layout__header'>
            <AImage
                classNameFigure={`layout__header_figure ${classHeader ? classHeader : ''}`}
                classNameImg='layout__header_img'
                classNameCaption='layout__header_caption'
                isFigcaption={isFigCaption}
                titleFigCaption={titleFigCaption}
                src={fichaPokerImg}
                alt='Pragma Poker'
                
            />
            {
                isSubHeader &&
                <>
                    <h2 className='layout__header_nameGame'>{nameGame}</h2>
                    <section className='layout__header_profile'>
                        <span className='layout__header_avatar' >{nameAvatar}</span>
                        <AButton
                            className='layout__header_invite'
                            isDisabled = {false}
                            textButton='Invitar jugadores'
                            type='button'
                            onClick={() => onClickInvitate()}
                        />
                    </section>
                </>
            }
          </header>
    )
}

export default Header