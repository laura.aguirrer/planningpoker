import { render } from 'enzyme';
import toJson from 'enzyme-to-json';
import {Header} from './Header.components';
import { Provider } from 'react-redux';
import store from 'store';


jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useLayoutEffect: jest.requireActual('react').useEffect,
  }));

describe('Test in MHeader Component', () => { 


    const wrapperHeaderLogueado = render(
        <Provider store={store}>
                <Header
                    nameAvatar='Jugador1'
                    isSubHeader
                    nameGame='Sprint-50'
                />
        </Provider>
    );
    const wrapperHeader = render(
        <Provider store={store}>
                <Header
                    isFigCaption
                    titleFigCaption='Crear Partida'
                />
        </Provider>
    );

    test('It shuld display correctly with component Header Logueado', () => { 
        expect(toJson(wrapperHeaderLogueado)).toMatchSnapshot();
    });

    test('It shuld display correctly with component Header No Logueado', () => { 
        expect(toJson(wrapperHeader)).toMatchSnapshot();
    });

    test('si estoy logueado deberia tener un nombre la partida', () => { 
        expect(toJson(wrapperHeaderLogueado).children[1].children[0]).not.toBe('')
    });

    test('si No estoy logueado deberia tener un un crear partida', () => { 
        expect('Crear Partida').toBe(toJson(wrapperHeader).children[0].children[1].children[0])
     })


});
