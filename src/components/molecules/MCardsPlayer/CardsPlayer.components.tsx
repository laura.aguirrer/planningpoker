import { FC } from 'react'
import { useAppSelector } from 'store/hooks'
import { createNameAvatarCharacters } from 'utils/helpers'
import { selectGame } from 'store/slices/Game'
import './CardsPlayer.components.scss'

interface ICardPlayer {
  value: number | string | null;
  namePlayer: string;
  numberPlayer: number;
  isViewer?: boolean;
}

export const CardsPlayer: FC<ICardPlayer> = ({ value, namePlayer, numberPlayer, isViewer }) => {
  const { game: { isVotation, isRevelationCards } } = useAppSelector(selectGame);
  return (
    <div className={
      ` main__table_poker_player 
          main__table_poker_player--${numberPlayer} 
          ${isViewer ? 'main__table_poker_player--viewer' : ''}
        `}
    >
      {
            isViewer ? 
              <span className='main__table_poker_label main__table_poker_label--viewer'>
                {createNameAvatarCharacters(namePlayer)}
              </span>
            : value !==  null
            ? isVotation 
                  ? 
                    isRevelationCards
                      ? <span className='main__table_poker_label'>{value}</span>
                      : <span className='main__table_poker_label main__table_poker_label--hidden'></span>
                  : <span className='main__table_poker_label'>{value}</span>
            : <span className='main__table_poker_label'> </span>
          }

      <span className={`main__table_poker_name ${isViewer ? 'main__table_poker_name--viewer' : ''}`}>{namePlayer}</span>
    </div>
  )
}

export default CardsPlayer