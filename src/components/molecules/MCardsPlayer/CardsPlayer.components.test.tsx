import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { CardsPlayer } from './CardsPlayer.components';
import { Provider } from 'react-redux';
import store from 'store';
import { IPlayer } from 'utils/interfaces';

describe('Test in Abutton Component', () => {
    const playersInitials: IPlayer[] = [
        {
            id: '1',
            namePlayer: 'AAA',
            rolPlayer: 'player',
            valuePlayer: null
        },
        {
            id: '2',
            namePlayer: 'BBB',
            rolPlayer: 'viewer',
            valuePlayer: null
        },
    ];

    const wrappeCardsPlayerA = shallow(
        <Provider store={store}>
            {playersInitials.map((player, index) => (
                <CardsPlayer
                    key={index}
                    namePlayer={player.namePlayer}
                    numberPlayer={index + 1}
                    isViewer={player.rolPlayer === 'viewer' ? true : false}
                    value={player.valuePlayer}
                />
            ))
            }

        </Provider>
    );

    const addPlayer = () => {
        const wrappeCardsPlayerB = shallow(  //local
            <Provider store={store}>
                {
                    playersInitials.map((player, index) => (
                        <CardsPlayer
                            key={index}
                            namePlayer={player.namePlayer}
                            numberPlayer={index + 1}
                            isViewer={player.rolPlayer === 'viewer' ? true : false}
                            value={player.valuePlayer}
                        />
                    ))
                }

            </Provider>
        );
        return { wrappeCardsPlayerB }
    }


    test('It shuld display correctly with component CardsPlayer A', () => {
        expect(toJson(wrappeCardsPlayerA)).toMatchSnapshot();
    });

    test('store add player', () => {
        playersInitials.push({ id: '3', namePlayer: 'CCC', rolPlayer: 'player', valuePlayer: null });
        const { wrappeCardsPlayerB } = addPlayer();
        expect(toJson(wrappeCardsPlayerB)).toMatchSnapshot();
    });

    // test('store add player ', () => { 
    //     playersInitials.push({id:'3',namePlayer:'CCC',rolPlayer:'player',valuePlayer:null});
    //     const {wrappeCardsPlayerB} = addPlayer();

    //     expect(toJson(wrappeCardsPlayerB)).toMatchSnapshot();
    // }); dentro de esta de donde saoco el rol y si vuelvo a meterlo en el snapshot


    test('deberia tener 2 jugadores inicialmente', () => {
        expect(2).toBe(toJson(wrappeCardsPlayerA).children.length)
        // console.log(toJson(wrappeCardsPlayerA).children)
    });

    // test('deberia tener 2 jugadores inicialmente', () => { 
    //     expect(2).toBe(toJson(wrappeCardsPlayerA).children.length)
    // }) que esta sucediendo acá poder hacer un cl

    test('deberia tener 3 jugadores inicialmente', () => {
        const { wrappeCardsPlayerB } = addPlayer();
        expect(3).toBe(toJson(wrappeCardsPlayerB).children.length)
    });
    // test('deberia tener 3 jugadores inicialmente', () => {
    //     const {wrappeCardsPlayerB} = addPlayer(); PRIMERO HAY QUE INICIALIZAR ESTO?

    //     expect(3).toBe(toJson(wrappeCardsPlayerB).children.length)
    // })

});

