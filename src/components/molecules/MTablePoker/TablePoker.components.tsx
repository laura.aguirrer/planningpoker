import { FC, useState, useEffect } from 'react';
import { AButton, ACountingVotes } from 'components/atoms';
import { CardsPlayer } from 'components/molecules';
import { useAppSelector, useAppDispatch } from 'store/hooks';
import {  selectGame, setStateVoted, setResetVotedPlayers, 
          setStateRevelateCards, setOperations } from 'store/slices/Game';
import { setResetCheckedCards } from 'store/slices/Auth'
import { isVoted, countValues } from 'utils/helpers'
import './TablePoker.components.scss';



interface ITablePoker {
  isShowOptionsTable?: boolean;
}

export const TablePoker: FC<ITablePoker> = ({ isShowOptionsTable = false }) => {
  const [isCountingVotes, setCountingVotes] = useState(false)
  const dispatch = useAppDispatch();
  const { game: { players, isVotation, isRevelationCards } } = useAppSelector(selectGame);


  useEffect(() => {
    if (isVoted(players)) {

      dispatch(setStateVoted({ isVotation: true }));
    }
  }, [players]);

  const onClickNewVotation = () => {
    dispatch(setResetVotedPlayers({ valuePlayer: null }));
    dispatch(setResetCheckedCards());
    dispatch(setStateVoted({ isVotation: false }));
    dispatch(setStateRevelateCards({ isRevelationCards: false }));
    dispatch(setOperations({operations:{isResult:false,valuesCount:[], average:0}}))

  }

  const onClickRevelate = () => {
    setCountingVotes(true);
    setTimeout(() => {
      dispatch(setStateRevelateCards({ isRevelationCards: true }));
      setCountingVotes(false);
      dispatch(setOperations({operations:{isResult:true,valuesCount:countValues(players), average:0}}))
    }, 3000);
  }


  return (
    <section className='main__table_poker'>
      <div className="main__table_poker_thumbail main__table_poker_thumbail--1"></div>
      <div className="main__table_poker_thumbail main__table_poker_thumbail--2"></div>
      <div className="main__table_poker_thumbail main__table_poker_thumbail--3"></div>

      {
        isCountingVotes
          ? <ACountingVotes

            />
          :
          <>
            {
              isVotation &&
              <AButton
                className='main__table_poker__btn_new_vote' type='button'
                textButton='Revelar cartas' onClick={() => onClickRevelate()}
              />
            }

            {
              isVotation && isRevelationCards &&
              <AButton
                className='main__table_poker__btn_new_vote' type='button'
                textButton='Nueva votación' onClick={() => onClickNewVotation()}
              />
            }
          </>
      }





      {
        isShowOptionsTable &&
        <>
          {
            players?.map(({ namePlayer, rolPlayer, valuePlayer }, index) => (
              <CardsPlayer
                key={index}
                isViewer={rolPlayer === 'viewer' ? true : false}
                namePlayer={namePlayer}
                numberPlayer={index + 1}
                value={valuePlayer} />
            ))
          }
        </>
      }

    </section>
  )
}

export default TablePoker