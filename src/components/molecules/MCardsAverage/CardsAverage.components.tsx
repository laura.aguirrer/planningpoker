import { useEffect } from 'react';
import { useAppSelector, useAppDispatch } from 'store/hooks'
import { selectGame, setAvarege } from 'store/slices/Game'
import {pluralOrSingular, getAverage} from 'utils/helpers'
import './CardsAverage.components.scss'


export const CardsAverage = () => {
  const dispatch = useAppDispatch();
  const { game: { operations: { valuesCount, average } } } = useAppSelector(selectGame)

  useEffect(() => {
    dispatch(setAvarege({avarage: getAverage(valuesCount)}))
    }, [valuesCount,dispatch])
    
  return (
    <article className='main__cards main__cards--average'>
      <section className='main__cards__container'>
        {
          valuesCount.map(({count,value }, index) => (
            <div key={index} className='main__cards__item'>
              <span className='main__cards__value'>{value}</span>
              <span className='main__cards__count' >{pluralOrSingular(count)}</span>
            </div>
          ))
        }

      </section>

      <section className='main__cards__promedio'>
        <div className='main__cards__promedio__container'>
          <h4 className='main__cards__promedio_title' >Promedio</h4>
          <span className='main__cards__promedio_value'> {average}</span>
        </div>
      </section>


    </article>
  )
}

export default CardsAverage