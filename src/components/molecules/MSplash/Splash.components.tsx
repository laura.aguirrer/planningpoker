import {AImage} from 'components/atoms'
import {fichaPokerImg, logoImg} from 'utils/assets/img'
import './Splash.components.scss'


export const Splash = () => {
  
  return (
    <div className='splash__container'>
        <AImage
            src={fichaPokerImg}
            alt='ficha poker'
            classNameFigure='splash__figure_ficha'
            classNameImg='splash__img_ficha'
        />
        <AImage
            src={logoImg}
            alt='Logotipo'
            classNameFigure='splash__figure_logo'
            classNameImg='splash__img_logo'
        />
    </div>

  )
}

export default Splash