import { FC, Dispatch } from 'react'
import { ALabel } from 'components/atoms';
import './Rols.components.scss';

interface IRols {
    role: string;
    setRol: Dispatch<React.SetStateAction<string>>;
}

export const Rols: FC<IRols> = ({setRol, role }) => {

    return (
        <>
        {/* Fragmanet */}
            <div className='start_player_form_rol_radio'>
                <ALabel text='Jugador' onClick={() => setRol('player')}
                    className='start_player_form_rol_label start_player_form_rol_label--j'
                />

                <span className='start_player_form_radio' onClick={() => setRol('player')}></span>
                {role === 'player' && <span className='start_player_form_radio--active'></span> }
            </div>

            <div className='start_player_form_rol_radio'>
                <ALabel text='Espectador' onClick={() => setRol('viewer')}
                    className='start_player_form_rol_label start_player_form_rol_label--e'
                />

                <span className='start_player_form_radio' onClick={() => setRol('viewer')}></span>
                {role === 'viewer' && 
                <span className='start_player_form_radio--active start_player_form_radio--active--e'></span>}
            </div>
        </>
    )
}

export default Rols