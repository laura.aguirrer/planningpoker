import { selectAuth, setChekedCards } from 'store/slices/Auth'
import { useAppSelector, useAppDispatch } from 'store/hooks';
import { setStateVotedByIdPlayer } from 'store/slices/Game'
import './CardsValues.components.scss'


export const CardsValues = () => {
  const dispatch = useAppDispatch();
  const { auth: { cardsValues, id, rolPlayer } } = useAppSelector(selectAuth);

  const onClickCheked = (pId: number | string, value: number | string) => {
    dispatch(setChekedCards({ id: pId, isCheked: true, value }))
    dispatch(setStateVotedByIdPlayer({ id, valuePlayer: value }))
  }

  return (
    <section className='main__cards'>
      {
        rolPlayer === 'player'
        &&
        <>
          <h3 className='main__cards__title'>Elige una carta <span className="main__cards__title--hand">👇</span></h3>
          <ul className='main__cards__ul'>
            {
              cardsValues?.map(({ isCheked, value, id }, index) => (
                <li
                  onClick={() => onClickCheked(id, value!)}
                  className={`main__cards__li ${isCheked ? 'main__cards__li--active' : ''}`}
                  key={index}>{value}</li>
              ))
            }
          </ul>
        </>

      }

    </section>
  )
}



export default CardsValues