import { useState, useEffect, FormEventHandler } from 'react';
import {useNavigate} from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import {useForm} from 'hooks'
import {AButton, ALabel, AInput} from 'components/atoms'
import {useAppDispatch} from 'store/hooks'
import {createRoom} from 'store/slices/Game/game.slice'
import './FormNewGame.components.scss'


export const FormNewGame = () => {

  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const {formValue,handleInputChange} = useForm(
        {defaultValues: {newGame: ''}}
  );
  const {newGame} = formValue;
  const [isDisabled, setDisabled] = useState(true)

  const newGameSubmit: FormEventHandler = (event) => {
    event.preventDefault();
    dispatch(createRoom({id:uuidv4(), nameRoom: newGame, players:[], operations:{isResult:false, average:0, valuesCount:[]}}))
    navigate('/start-player');
  } 

  useEffect (() => {
    if(newGame.length >= 9) return setDisabled (false);
    setDisabled(true);
  }, [newGame]);
  
  
  return (
    <form className='main__form_newGame' autoComplete='off'
        onSubmit={newGameSubmit}
    >
            <ALabel
              className='main__form_newGame_label'
              text= 'Nombra la partida'
              htmlFor='name'
            />      
            <AInput
             className='main__form_newGame_input'            
             type='text'
             name='newGame'
             value={newGame}
             onChange= {handleInputChange}
             id='name'
             min='9'
             isRequired={true}
            />         
            <AButton 
               type='submit'
               textButton='Crear partida'
               className= {`main__form_newGame_button ${isDisabled ? 'main__form_newGame_button--disable' : '' }`}               
               isDisabled={isDisabled}
            />
    </form>
  )
}


export default FormNewGame