import {useState, useEffect} from 'react'
import {useForm} from 'hooks'
import { v4 as uuidv4 } from 'uuid';

import {AInput,AButton, ALabel} from 'components/atoms';
import {Rols} from 'components/molecules';
import {useAppDispatch} from 'store/hooks'
import {setAuthenticate} from 'store/slices/Auth'
import {addPlayers} from 'store/slices/Game'
import {cardsValues} from 'utils/data'
import './FormStartPlayer.components.scss';

export const FormStartPlayer = () => {
  const dispatch = useAppDispatch();
  const [isDisable, setIsDisable] = useState(false);
  const [role, setRole] = useState('');
  const {formValue,handleInputChange} = useForm({ defaultValues: {namePlayer: ''} });
  const {namePlayer} = formValue;



  useEffect(() => {
    validateForm()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [role, namePlayer])


  const handleSubmit = () => {
    if(isDisable) return;
    // dispatch
    const id = uuidv4();
    dispatch(setAuthenticate({isAuthenticate: true,namePlayer, id, cardsValues,rolPlayer:role }));
    dispatch(addPlayers({id,namePlayer,rolPlayer: role, valuePlayer: null}));
    // redireccion
  }

  const validateForm = () =>  {
    if (role  === '' || namePlayer === '' ) {
      return setIsDisable(true);
    }
    setIsDisable(false);
  }

  return (
    <div className='start_player_form' >
      <section className='start_player_form_name'>
        <ALabel className='start_player_form_label_name ' text='Tu nombre' />
        <AInput name='namePlayer' value={namePlayer} onChange={handleInputChange}  
                type='text' className='start_player_form_text' autoComplete='off'
        />
      </section>
      <section className='start_player_form_rol'>
        <Rols role={role} setRol={setRole} />
      </section>
      <AButton
          type='button'
          textButton='Continuar'
          className={`btn start_player_form_btn ${isDisable ? 'start_player_form_btn--disable' : ''}`}
          onClick={() => handleSubmit()}
          isDisabled={isDisable}
        />
    </div>
  )
}

export default FormStartPlayer