import { FC } from 'react'
import { TablePoker, CardsValues, CardsAverage } from 'components/molecules'
import { useAppSelector } from 'store/hooks'
import { selectGame } from 'store/slices/Game'
import './TablePokerCardsValues.components.scss'



interface ITablePoker {
  isShowOptionsTable?: boolean;
}

export const TablePokerCardsValues: FC<ITablePoker> = ({ isShowOptionsTable = false }) => {
  const { game: { operations: { isResult } } } = useAppSelector(selectGame)

  return (
    < >
      <TablePoker isShowOptionsTable={isShowOptionsTable} />
      {
        isResult
          ? <CardsAverage />
          :  <CardsValues/>
      }
    </>
  )
}


export default TablePokerCardsValues