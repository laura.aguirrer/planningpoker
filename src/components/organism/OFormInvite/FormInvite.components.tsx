import { useState} from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { AButton, AImage, AInput } from 'components/atoms';
import { setModal, selectGame } from 'store/slices/Game'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import { iconClose } from 'utils/assets/img';
import { BASE_URL } from 'utils/config/constants'
import './FormInvite.components.scss'


export const FormInvite = () => {
  const {game: {id}}= useAppSelector(selectGame)
  const [textBtn, setTextBtn] = useState('Copiar link');
  // const [domain, setDomain] =  useState<string>(BASE_URL.domain + id);
  const domain = BASE_URL.domain + id;
  const distpach = useAppDispatch();

  const onClickInvitate = () => {
    distpach(setModal({ isModal: false, typeModal: '' }));
  }

  return (
    <form className='layout__form__invite'>
      <div className='layout__form__invite_header'>
        <h3 className='layout__form__invite_tittle'>Invitar Jugadores</h3>
        <AImage src={iconClose} alt='icon-close'
          classNameFigure='layout__form__invite_icon_close'
          classNameImg='layout__form__invite_icon_image'
          onClickImage={() => onClickInvitate()}
        />
      </div>
      <div className='layout__form__invite_main'>
        <AInput className='layout__form__invite_input' id='link' readOnly
          type='url' name='link' defaultValue={domain} />

        <CopyToClipboard text={domain} onCopy={()=> setTextBtn('Copiado.')}>
          <AButton className='layout__form__invite_btn'
            type='button' textButton={textBtn}  />
        </CopyToClipboard>
      </div>
    </form>
  )
}

export default FormInvite