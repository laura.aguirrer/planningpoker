import { FC } from 'react';
import {Header} from 'components/molecules'
import {FormStartPlayer, FormNewGame} from 'components/organism'
import './Layout.components.scss'
import FormInvite from '../../organism/OFormInvite/FormInvite.components';

interface ILayout {
    isFigCaption?: boolean;
    isSubHeader?: boolean;
    titleFigCaption?: string;
    nameGame?: string;
    nameAvatar?: string;
    isModal?: boolean;
    typeModal?: ''| 'start-player' | 'invite';
    classHeader?: string;
}


export const Layout: FC<ILayout> = (
    {children, isFigCaption = false, isSubHeader=false, classHeader,
     titleFigCaption, nameGame = '', nameAvatar = '', isModal=false, typeModal}
    ) => {
 
        return (
        <div className='layout__container'>
                <Header isFigCaption= {isFigCaption}  
                isSubHeader = {isSubHeader} 
                titleFigCaption={titleFigCaption}
                nameGame= {nameGame}
                nameAvatar= {nameAvatar}
                classHeader={classHeader}
                />          
                <main className='layout__main'>
                    {children}
                </main>
                {
                    isModal &&
                    <section className='layout__modal'>
                        {
                            typeModal === 'start-player'&& 
                            <FormStartPlayer/>
                            
                        }
                        {
                             typeModal === 'invite'&& 
                             <FormInvite/>
                        }
                        
                    </section>
                }
            </div>
  )
}

export default Layout