import './ACountingVotes.components.scss'

export const ACountingVotes = () => {
  return (

    <section className ="table-poker__loading">
      <div className ="table-poker__loading-heade">
        <span className ="table-poker__item-load"></span>
        <span className ="table-poker__item-load"></span>
        <span className ="table-poker__item-load"></span>
        <span className ="table-poker__item-load"></span>
        <span className ="table-poker__item-load"></span>
      </div>
      <div className ="table-poker__loading-footer">
        <h3 className ="table-poker__loading-footer_title">Contando votos</h3>
      </div>
    </section>
  )
}



export default ACountingVotes