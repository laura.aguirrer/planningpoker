import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { fichaPokerImg } from 'utils/assets/img'
import AImage from './AImage.components';

describe('Test in AImage Component', () => {

    const wrapperAImage = shallow(
        <AImage
            src={fichaPokerImg}
            alt='Ficha Logo'
            isFigcaption
            titleFigCaption='Crear partida'

        />
    );


    test('It shuld display correctly with component AImage', () => {
        expect(toJson(wrapperAImage)).toMatchSnapshot();
    });

    test('It should enter the src', () => {
        expect(toJson(wrapperAImage).props.src).not.toBe('')
        // console.log(toJson(wrapperAImage).children);
    });
    test('It should enter the title', () => {
        console.log(toJson(wrapperAImage).children)
        expect(toJson(wrapperAImage).children[0].props.title).not.toBe('') 
    });
    test('It should enter the alt', () => {
        expect(toJson(wrapperAImage).children[0].props.alt).not.toBe('') 
    });
    test('It shuld display correctly the txt "Crear partida"', () => { 
        expect('Crear partida').toBe(toJson(wrapperAImage).children[1].children[0])
        // console.log(toJson(wrapperAImage).children[1].children[0])
    });
});