import { FC, MouseEventHandler } from 'react';
import './AImage.components.scss'

interface IAImage {
  classNameFigure?:string;
  classNameCaption?: string;
  classNameImg?:string;
  src: string;
  alt: string;
  title?: string;
  isFigcaption?: boolean;
  titleFigCaption?: string;
  onClickImage?:MouseEventHandler<HTMLImageElement>;
}

export const AImage: FC<IAImage> = (
  { classNameFigure,classNameImg,classNameCaption,src, alt, 
    title, isFigcaption=false,titleFigCaption, onClickImage}
  ) => {
  return (
    <figure className={classNameFigure}>
      <img className= {classNameImg}  src={src} alt={alt} title={title} onClick={onClickImage}/>
      {
        isFigcaption &&  <figcaption className={classNameCaption ? classNameCaption: ''}>{titleFigCaption}</figcaption>
      }
    </figure>
  )
}

export default AImage