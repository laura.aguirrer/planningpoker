import {ChangeEventHandler, FC, HTMLInputTypeAttribute} from 'react'

interface IAInput {
    className:string;
    name?: string;
    value?: string;
    onChange?:ChangeEventHandler<HTMLInputElement>;
    type: HTMLInputTypeAttribute;
    id?: string;
    min?: string | number;
    max?: string | number;
    isRequired?: boolean
    isChecked?:boolean;
    isDisable?: boolean
    readOnly?: boolean;
    defaultValue?: string | number | readonly string[] | undefined;
    defaultChecked?: boolean | undefined;
    autoComplete?: 'off'
}

export const AInput: FC<IAInput> = (
  { className, type, name, value, onChange, id, min, max, autoComplete,
    isRequired=false, isChecked=false, isDisable=false, readOnly=false, defaultValue, defaultChecked}) => {
  return (
    <input 
     className={className}
     id= {id}
     type={type} 
     value= {value}
     onChange={onChange}
     checked={isChecked}
     min= {min}
     max={max}
     name={name}     
     required={isRequired}
     disabled={isDisable}
     readOnly= {readOnly}
     defaultValue={defaultValue}
     defaultChecked={defaultChecked}
     autoComplete={autoComplete}
    />
  )  
}



export default AInput