import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import AInput from './AInput.components';

describe('Test in AInput Component', () => {

    const wrapperAInput = shallow(
        <AInput
            className='input--test'
            type='text'
            id= 'name'
            min='9'
            value='Sprint 23'
        />
    );

    test('It shuld display correctly with component AInput', () => {
        expect(toJson(wrapperAInput)).toMatchSnapshot();
    });

    test('It shuld display correctly with component AInput type text', () => {
        let namePartida = 'Sprint 23';
        if (namePartida !== '' && namePartida.length >= 9) {
            expect('Sprint 23').toEqual(toJson(wrapperAInput).props.value);
        }
    });

});