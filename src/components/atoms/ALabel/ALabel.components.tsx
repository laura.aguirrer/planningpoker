import { FC, MouseEventHandler } from 'react';
import './ALabel.components.scss'

interface IALable {
    text: string;
    className: string;
    htmlFor?: string
    onClick?: MouseEventHandler<HTMLLabelElement>
}

export const ALabel: FC<IALable> = ({text, className, htmlFor, onClick}) => {
  return (
    <label className={className} htmlFor={htmlFor} onClick={onClick}> 
        {text}
        </label>
  )
}



export default ALabel