import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import {ALabel} from './ALabel.components';

describe('Test in ALabel Component', () => { 

    const wrapperALabel = shallow(
        <ALabel
            className='label--test'
            text='Nombra la partida'            
        />
    );

    test('It shuld display correctly with component ALabel', () => { 
       expect(toJson(wrapperALabel)).toMatchSnapshot();
    });

    test('It shuld display correctly with component ALabel with the txt "Nombra la partida"', () => { 
        expect('Nombra la partida').toBe(toJson(wrapperALabel).children[0])
    });


});