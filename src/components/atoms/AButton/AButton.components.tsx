import { FC, MouseEventHandler } from 'react';
import './AButton.components.scss';

interface IAButton {
    type: 'button' | 'submit';
    textButton: string;
    className: string;
    isDisabled?:boolean;
    onClick?: MouseEventHandler<HTMLButtonElement>;
}

 export const AButton: FC<IAButton> = ({type, textButton, className, isDisabled=false , onClick} ) => {
  return (
    <button
        type={type}
        className= {`btn ${className}`} 
        disabled={isDisabled}
        onClick={onClick}
    >{textButton}
    </button>
  )
}


export default AButton