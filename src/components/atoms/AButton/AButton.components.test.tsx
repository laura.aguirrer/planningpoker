import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import {AButton} from './AButton.components';

describe('Test in Abutton Component', () => { 


    const wrapperAButtonA = shallow(
        <AButton
            type='button'
            className='btn btn--success'
            textButton='Crear Partida'
            
        />
    );

    const wrapperAButtonB = shallow(
        <AButton
            type='button'
            className='btn btn--success'
            textButton='Crear Partida'
            isDisabled
        />
    );


    test('It should display correctly with component AButton A', () => { 
        expect(toJson(wrapperAButtonA)).toMatchSnapshot();
    });

    test('It should display correctly with component AButton B', () => { 
        expect(toJson(wrapperAButtonB)).toMatchSnapshot();
    });
   
    test('It should display correctly with component AButton B disabled', () => { 
        const componentDisable: boolean | undefined = 
        toJson(wrapperAButtonB).props.disabled;

        expect(true).toBe(componentDisable)
    });

    test('It should display correctly with component AButton B not disabled', () => { 
        let namePartida = '';
        const txtNotEmpty = toJson(wrapperAButtonB);
        namePartida = 'AAAA'
        if (namePartida !== '' && namePartida.length >= 4) {
            expect(false).toBe(txtNotEmpty.props.disabled = false);
        }else {
            expect(true).toBe(txtNotEmpty.props.disabled);
        }
    });

});


// mock o 'mokear' datos  
// wrapper "dibujo de mi componente"